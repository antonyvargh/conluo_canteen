
$(document).ready(function(){
    $(".menu_toggle").click(function(e){
        $(this).toggleClass('opened');
        $('.main_nav').toggleClass('nav_show');
    });

    $(".footer_row h4").click(function(e){
      e.preventDefault();
      $(this).toggleClass('accordion_open');
      $(this).closest(".col").find(".links_wrap").slideToggle(100);
      $(this).closest(".address_col").find(".links_wrap").slideToggle(100);

      
    })

    var scrollVal=0;
    $( window ).scroll(function() {
      scrollVal = $(window).scrollTop();
        if(scrollVal > 100){
          $(".header").addClass('sticky');
         
        }else{
          $(".header").removeClass('sticky');
         
        }
        
    });

    // menu scroll
    var section;
    $(".menu_link").click(function(e){
      if($(this).is('a[href^="#"]')) {
        e.preventDefault();
        section = $(this).attr("href");
        if(section == '#features'){
          topval = 64;
        }else{
          topval = 0;
        }
        $('html, body').animate({
          scrollTop: $(section).offset().top - topval
        }, 500);
        setTimeout(function() {
          $(".menu_toggle").removeClass("opened");
          $(".main_nav").removeClass("nav_show");
        }, 500);
        
      }
    });

    $("#contact_form").validate({
      rules:{ 
        name:"required",
        phone:"required",
        email:"required",
        city:"required",
        message:"required",
      },
      submitHandler: function(form) {
        contactData = $("#contact_form").serialize();
        sendAppoinment(contactData);
        console.log(contactData);
      }
    });
    
    function sendAppoinment(ctData){
      $.ajax({
        url:'sendmail.php',
        data: ctData,
        type:'post',
        success: function(data){
          //console.log(data);
      
          setTimeout(function(){
         
            $("#contact_form").trigger('reset');
            
          },3000);						
        },
        error: function(){
        }
      });
    }

});
